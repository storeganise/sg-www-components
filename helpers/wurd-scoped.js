/**
 * Creates an object of Wurd content helper functions, scoped to a particular section path
 * This means that a component doesn't need to know the parent path, and can use a shorter path 
 * e.g. 'title' instead of 'section.title'
 */
module.exports = function(wurd, sectionPath) {
  const obj = {
    path: itemPath => itemPath ? [sectionPath, itemPath].join('.') : sectionPath
  };

  ['get', 'map', 'el'].forEach(fnName => {
    obj[fnName] = function(itemPath, ...rest) {
      const fullPath = itemPath ? [sectionPath, itemPath].join('.') : sectionPath;

      return wurd[fnName](fullPath, ...rest)
    }
  });

  return obj;
};
