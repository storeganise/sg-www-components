const React = require('react');


module.exports = ({content, className, style, showPrivacy = false}) => {

  const t = content.get;

  const phoneLink = 'tel:' + (t('phone') || '').replace(/ /g, '');


  return (
    <footer data-wurd-block={content.path()} className={className} style={style}>
      <div className="container">
        <div className="row">
          <div className="col-sm-12 text-center">
            <h2 data-wurd=".title">{t('title')}</h2>

            <ul className="list-inline" style={{marginTop: 20}}>
              <li><a href={phoneLink}><i className="fa fa-fw fa-2x fa-phone-square"></i> <span data-wurd=".phone">{t('phone')}</span></a></li>
              <li><a target="_blank" href={`mailto:${t('email')}`}><i className="fa fa-fw fa-2x fa-envelope-square"></i> <span data-wurd=".email">{t('email')}</span></a></li>
            </ul>
          </div>
        </div>
        <div className="row" style={{marginTop: 40}}>
          <div className="col-sm-6">
            <ul className="links list-inline container">
              {showPrivacy &&
                <li><a data-wurd=".privacy" href="/privacy">{t('privacy')}</a></li>
              }
              <li><a data-wurd=".terms" href="/terms">{t('terms')}</a></li>
            </ul>
          </div>
          <div className="col-sm-6">
            <div className="text-right"><a href="http://storeganise.com" target="_blank">Powered by Storeganise valet storage software</a></div>
          </div>
        </div>
      </div>
    </footer>
  );

};
