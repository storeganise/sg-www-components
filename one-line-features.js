const React = require('react');

const Markdown = require('./markdown');


module.exports = ({ content }) => {

  const t = content.get;


  return (
    <div 
      className="one-line-features" 
      data-wurd-block={content.path()}
    >
      <div data-wurd-md=".intro"><Markdown>{t('intro')}</Markdown></div>
      <ul data-wurd-list=".items" className="row list-unstyled">
        {content.map('items', (item, id) => 
          <li key={id} data-wurd-block={id} data-wurd-md=".text" className="col-sm-6">
            <Markdown>{item.text}</Markdown>
          </li>
        )}
      </ul>
      <div data-wurd-md=".outro"><Markdown>{t('outro')}</Markdown></div>
    </div>
  );

};
