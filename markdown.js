const React = require('react');
const markdown = require('marked');


module.exports = ({children = ''}) => {
  return (
    <span dangerouslySetInnerHTML={{__html: markdown(children)}} />
  );
};
