const React = require('react');


module.exports = ({urls, content, className, style}) => {

  const t = content.get;
  
  return (
    <header className={className} style={style}>
      <nav id="nav" className="navbar navbar-fixed-top" role="navigation">
        <div className="container-fluid">
          <div className="navbar-header">

            <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="nav .collapse">
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>

            <a className="navbar-brand" href="/">{t('brand')}</a>
          </div>

          <div data-wurd-block={content.path()} className="collapse navbar-collapse navbar-right">
            <ul className="nav navbar-nav">
              <li className="hidden-sm navbar-divider"><a className="email" target="_blank" href={`mailto:${t('email')}`}><i className="glyphicon glyphicon-envelope"></i> <span data-wurd=".email">{t('email')}</span></a></li>
              <li className="hidden-sm"><a data-wurd=".howWorks" className="how-it-works-navlink" href="/#how-it-works">{t('howWorks')}</a></li>
              <li><a data-wurd=".pricing" className="hidden-sm pricing-navlink" href="/#pricing">{t('pricing')}</a></li>
              <li><a data-wurd=".help" href="/help">{t('help')}</a></li>
              <li><a data-wurd=".login" href={urls.login}>{t('login')}</a></li>
              <li className="signup"><a data-wurd=".signup" href={urls.getStarted}>{t('signup')}</a></li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  );

};
