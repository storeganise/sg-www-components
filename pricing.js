const React = require('react');

const Markdown = require('./markdown');


module.exports = ({urls, content, showPlanButtons = false}) => {

  const t = content.get;


  return (
    <div 
      className="pricing" 
      data-wurd-block={content.path()}
    >
      <h2 data-wurd=".title">{t('title')}</h2>
      <div data-wurd-list=".items">
        {content.map('items', (item, id) => 
          <div key={id} data-wurd-block={id} className="row item">
            <div className="col-sm-4 image">
              <img data-wurd-img=".image" src={item.image} style={{width: 220}} />
            </div>
            <div className="col-sm-5 content">
              <h3 data-wurd=".title">{item.title}</h3>
              <div data-wurd-md=".text"><Markdown>{item.text}</Markdown></div>
            </div>
            <div className="col-sm-3 text-center">
              <div data-wurd-md=".price" className="price">
                <Markdown>{item.price}</Markdown>
              </div>

              {showPlanButtons &&
                <span data-wurd-obj={id} data-wurd-obj-props="plan,button">
                  <a className="btn btn-primary" href={`${urls.getStarted}?plan=${item.plan}`}>{item.button}</a>
                </span>
              }
            </div>
          </div>
        )}
      </div>
    </div>
  );

};
