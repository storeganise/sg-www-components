const React = require('react');


function getColSize(numItems) {
  if (numItems === 4) return 6;

  return 4;
};


module.exports = ({ content }) => {

  const t = content.get;

  // Decide how wide each item should be based on the number to display
  const numItems = Object.keys(content.get('steps', {})).length;
  const colSize = getColSize(numItems);


  return (
    <div 
      className="how-it-works container"
      data-wurd-block={content.path()}
    >
      <h2 data-wurd=".title">{t('title')}</h2>

      <ul data-wurd-list=".steps" className="row">
        {content.map('steps', (item, id) => 
          <li key={id} data-wurd-block={id} className={`col-sm-${colSize} icon`}>
            <img data-wurd-img=".image" src={item.image} alt="" />
            <h3 data-wurd=".title">{item.title}</h3>
            <p data-wurd=".text">{item.text}</p>
          </li>
        )}
      </ul>
    </div>
  );
  
};
