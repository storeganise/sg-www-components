const React = require('react');

const Markdown = require('./markdown');


module.exports = ({urls, content}) => {

  const t = content.get;

  return (
    <div 
      className="hero" 
      data-wurd-block={content.path()} 
      data-wurd-bgimg=".image"
      style={{backgroundImage: `url(${t('image')})`}}
    >
      <div className="container">
        <div className="row">
          <div className="col-sm-12">
            <div data-wurd-md=".title"><Markdown>{t('title')}</Markdown></div>

            <a data-wurd=".button" href={urls.getStarted} className="btn btn-large btn-primary">{t('button')}</a>
          </div>
        </div>
      </div>
    </div>
  );

};
