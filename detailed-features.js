const React = require('react');

const Markdown = require('./markdown');


module.exports = ({content}) => {

  const t = content.get;


  return (
    <div 
      className="detailed-features" 
      data-wurd-list={content.path('items')}
    >
      {content.map('items', (item, id) => 
        <div 
          className="detailed-features__item"
          key={id} 
          data-wurd-block={id} 
          data-wurd-bgimg=".image" 
          style={{backgroundImage: `url(${item.image})`}}
        >
          <h2 data-wurd=".title">{item.title}</h2>
          <div data-wurd-md=".text"><Markdown>{item.text}</Markdown></div>
        </div>
      )}
    </div>
  );

};
