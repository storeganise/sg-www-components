const React = require('react');

const Markdown = require('./markdown');


module.exports = ({ content }) => {

  const t = content.get;


  return (
    <div 
      className="text-block"
      data-wurd-md={content.path()}
    >
      <Markdown>{t() || '## Section title  \r\n### Question?  \r\nAnswer'}</Markdown>
    </div>
  );

};
