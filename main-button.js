const React = require('react');


module.exports = ({url, content, style}) => {

  const t = content.get;


  return (
    <section className="main-button" style={style}>
      <a href={url} className="btn btn-primary" data-wurd={content.path('text')}>{t('text')}</a>
    </section>
  );

};
