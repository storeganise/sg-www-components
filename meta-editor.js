const React = require('react');

module.exports = ({pageId}) => {
  if (!pageId) return null;

  return (
    <div className="meta-editor" data-wurd-obj={`${pageId}.meta`} data-wurd-obj-props="title,description,keywords">
      Meta
    </div>
  );
};
