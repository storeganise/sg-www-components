const React = require('react');
const wurdScoped = require('./helpers/wurd-scoped');

const Header = require('./header');
const Footer = require('./footer');
const MetaEditor = require('./meta-editor');


module.exports = ({wurd, pageId, urls, children}) => {

  const t = function(path) {
    return wurd.text(path);
  }

  return (
    <html lang="en">
      <head>
        <meta charSet="utf-8" />

        <meta name="description" content={t('common.meta.description')} />
        <meta name="keywords" content={t('common.meta.keywords')} />

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />

        <meta httpEquiv="x-ua-compatible" content="IE=edge" />

        <link rel="shortcut icon" href="/images/icon.png" />

        <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/css/main.css" />

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" />

        <title>{t(`${pageId}.meta.title`)}</title>
      </head>
      <body>
        <Header 
          urls={urls}
          content={wurdScoped(wurd, 'common.nav')}
        />

        {children}

        <Footer
          urls={urls}
          content={wurdScoped(wurd, 'common.footer')}
        />

        <script src="/js/jquery.min.js"></script>
        <script src="/bootstrap/js/bootstrap.min.js"></script>

        {wurd.editMode &&
          <div>
            <script src="https://edit-v3.wurd.io/widget.js" data-app={wurd.app}></script>
            
            <MetaEditor pageId={pageId} />
          </div>
        }
      </body>
    </html>
  );

};
